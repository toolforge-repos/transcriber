module.exports = {
	"env": {
		"browser": true,
		"es2021": true
	},
	"extends": [
		"eslint-config-wikimedia",
		"plugin:react/recommended"
	],
	"globals": {
		"process": "readonly",
	},
	"parser": "@babel/eslint-parser",
	"parserOptions": {
		"requireConfigFile": false,
		"babelOptions": {
			"plugins" : [
				"@babel/plugin-proposal-class-properties",
			],
			"presets": [
				"@babel/preset-react",
			]
		}
	},
	"plugins": [
		"react",
	],
	"rules": {
		"indent": [
			"error",
			"tab"
		],
		"linebreak-style": [
			"error",
			"unix"
		],
		"quotes": [
			"error",
			"double"
		],
		"semi": [
			"error",
			"always"
		]
	},
	"settings": {
		"react": {
		  "version": "detect",
		},
	},
};

# Transcriber

The transcriber is intended to be a gadget that provides an On-Screen Keyboard (OSK) for facilitating easier transcription on Balinese and other Wikisources.

## Important Links
- [Toolforge Tool](https://toolsadmin.wikimedia.org/tools/id/transcriber) 
- [Phabricator Board](https://phabricator.wikimedia.org/tag/transcriber/)
- [Contributors](./CONTRIBUTORS.md)

## Requirements

*To be detailed*

## Using the Transcriber on Balinese Wikisource 

### Removing redundant wrapper code from `common.js`

Existing users of the transcriber on Balinese Wikisource have custom Javascript code in their `common.js` file that runs the transcriber. However, we have now devised an easier and more efficient method for the same. All of the required wrapper code has been integrated into the transcriber web app, deployed on Toolforge, and has been configured as a gadget on Balinese Wikisource.

Existing users will be required to remove the unnecessary, and repetitive, Javascript code from their `common.js`. Since the `common.js` can contain code related to various functionalities, for purposes of clarity the following are the names of the functions related to the transcriber that are to be removed.

- `transcriberLoad()`
- `transcriberEdit()`
- `transcriberView()`
- `getIaFile()`
- `getCommonsData()`
- `getTransliterationVariant()`
- `getTransliterateApi()`
- `addTransliteration()`
- `fixPagenums()`
- `addTransliterationForPages()`
- `fixProofreadPageCss()`
- `fixMainCss()`
- The Mediawiki hook that calls one or more of the above functions also needs to be deleted. It often begins with `mw.hook('wikipage.content').add(...)`. 

> Note: Please be careful not to remove any code apart from what is related to Transcriber as this may hamper any other scripts you may have written.

### Use as a user script
The transcriber can now be used as a user script by including the following code snippet in your `common.js` file.
```bash
mw.loader.load('https://transcriber.toolforge.org/static/css/transcriber.css', 'text/css');
mw.loader.load('https://transcriber.toolforge.org/static/js/transcriber.js');
```

### Use as a gadget
The transcriber can also be used as a gadget by enabling it from the `Gadgets` section on the `Special:Preferences` page.

> Note: Do not enable the gadget AND put the code snippet in your `common.js` at the same time as this may produce unintended results.

## More about the tool
The transcriber interface allows for a more seamless transcription experience for users. It overlays on top of the regular wiki edit page and reads and writes to the edit box when opened and closed. It is designed to be an accompaniment for the [ProofreadPage extension](https://www.mediawiki.org/wiki/Extension:Proofread_Page) and is a desktop-based interface.

The transcriber consists of three sections:

- The image to be transcribed, which can be zoomed in and out
- A text edit box that displays the current working text of the transcriber
- An on-screen keyboard

The transcriber reads from the `<transcription>` tag within the edit box, and when closed, writes back to the tag. After the transcriber is closed, the user must manually save the wiki edit.

## Keyboards available
Currently, the keyboard is available only for the Balinese language. More updates and improvements are in the pipeline.

### Balinese Keyboard

  - The Balinese onscreen keyboard is designed to type any characters found in Balinese, while preventing the typing of malformed sequences.
  - To type a consonant-vowel syllable, first type the consonant, and then the vowel marks for that consonant will be displayed in the upper right section.
  - Type all vowels after their consonant (in spoken order), even if they are written to the left of the consonant, like "ᬳᬾ".
  - To type consonant clusters, type the first consonant, then adeg-adeg (in the vowel marks section), then the following consonant. The keyboard will adjust to demonstrate the character to be typed.
  - If an explicit adeg-adeg is needed in a place that would normally cause a consonanant cluster (such as "ᬦ᭄‌ᬪ"), type the first consonant, then the adeg-adeg, then a Zero-Width Non-Joiner (the key to the right of the space bar), then the next consonant. The Zero-Width Non-Joiner blocks the adeg-adeg from creating a consonant cluster.
  - Sanskrit-specific consonants and less common vowel marks can be found by pressing the shift key.
  - To type numbers and punctuation marks, press the bottom-left key "᭗᭘᭙".

## Implementation

*To be detailed*
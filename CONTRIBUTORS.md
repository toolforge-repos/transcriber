# Information for Contributors

## Local installation

This tool has been created as a React app, designed to be used as a user script/gadget on Wikisource.

### Prerequisites
It is required to have Node v14.21.1 [(get it here)](https://nodejs.dev/en/download/) installed on your system for the tool to run as expected. Also have `nvm` installed on your system to manage node versions effectively.

### Get it running
- Clone the repository from [Gitlab](https://gitlab.wikimedia.org), from under the `toolforge-repos` project
	- Using SSH
		```bash
		git clone git@gitlab.wikimedia.org:toolforge-repos/transcriber.git
		```
	- Using HTTPS
		```bash
		git clone https://gitlab.wikimedia.org/toolforge-repos/transcriber.git
		```
- Once the repository is cloned successfully, move into the root directory and install the necessary dependencies for the app 
	- Note: Running the `nvm use` command ensures that the node version specified in the `.nvmrc` file is used
```bash
cd transcriber
nvm use
npm install
```
- Resolve any peer dependency conflicts, if any, to ensure proper installation of all dependencies
- After all the dependencies have been installed successfully, run the `start` script to see the tool live on your system
```bash
npm start
```
Go to `http://localhost:3000` on your browser and the transriber (with the on screen keyboard) should now be visible!

## Creating a production build

Even though it is a normal React app, for the purposes of being deployed as a user-script, and possibly a gadget later, there is a slight modification to the bundling of the static files. 

Run the `build` script that triggers the bash script at `script/build`. It does two things:
 - runs the `prod` script 
 - compiles all the `javascript` files into `transcriber.js` and the `css` files to `transcriber.css` 
```bash
npm run build
```

## Testing locally
*To be detailed*

## Deploying the tool
*To be detailed*

## Using it on Wikisource

For instructions on how to use the tool on Wikisource as a user script, please refer to the instructions in the README [here](https://gitlab.wikimedia.org/toolforge-repos/transcriber#installation).
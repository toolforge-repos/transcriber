import "react-app-polyfill/stable";
import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import * as serviceWorker from "./serviceWorker";

var variant = "ban-x-pku";

function fixProofreadPageCss() {
	var sheet = document.styleSheets[ 0 ];
	sheet.insertRule( "[lang='ban-Bali'] .pagetext { text-align: start !important; word-break: break-all !important; overflow-wrap: anywhere !important; }" );
}

function getTransliterationVariant( mw ) {
	const userVariant = mw.user.options.get( "variant-ban" );
	if ( userVariant && userVariant !== "ban" ) {
		variant = userVariant;
	}
}

function addTransliteration( page, elt, where, api ) {
	api.get( {
		action: "parse",
		prop: "text",
		text: "==== Auto-transliteration ====\n<span lang='ban'><langconvert from='ban-bali' to='' + variant + ''>{{:" + page + "}}</langconvert></span>",
		contentmodel: "wikitext",
		disablelimitreport: 1,
		disableeditsection: 1,
		formatversion: 2
	} )
		.then( function ( res ) {
			if ( !res.error ) {
				// var div = window.$(res.parse.text)[where](elt);
			}
		} );
}

const transcriberLoad = () => {
	const transcriber = document.createElement( "div" );
	document.body.appendChild( transcriber );
	ReactDOM.render( <App {...window.transcriberData} />, transcriber );
};

function transcriberEdit( title, mw ) {
	var filename, pagenum;
	var match = title.match( /^(.+)\/([0-9]+)$/ );
	if ( match ) {
		filename = match[ 1 ];
		pagenum = match[ 2 ] - 1;
	} else {
		filename = title;
		pagenum = 0;
	}

	var iaFile = getIaFile( filename );
	var imageUrl = document.querySelector( ".prp-page-image img" ).getAttribute( "src" );
	if ( !imageUrl ) {
		return;
	}

	var textbox = document.getElementById( "wpTextbox1" );
	if ( !textbox ) {
		return;
	}
	textbox.style.fontFamily = "Noto Sans Balinese, Noto Serif Balinese, Vimala, monospace";

	getCommonsData( filename, pagenum, mw ).then( function ( data ) {
		if ( !data ) {
			return;
		}
		var imageData = data.iiifImageData;
		window.transcriberData = {
			mode: "edit",
			textbox: textbox,
			imageUrl: imageUrl,
			leaf: pagenum,
			iaFile: iaFile,
			iaId: data.iaId,
			iiifBaseUrl: "https://iiif.archivelab.org/iiif",
			iiifDimensions: imageData ? ( imageData.all || imageData.pages[ pagenum ] ) : null,
			script: "bali",
			language: "ban-bali",
			variant: variant
		};
		transcriberLoad();
	} );
}

function getIaFile( filename ) {
	var match = filename.match( /^Bali-lontar-(.+)\.pdf$/i );
	return match ? match[ 1 ] : null;
}

function getCommonsData( filename, pagenum, mw ) {
	var api = new mw.ForeignApi( "https://commons.wikimedia.org/w/api.php" );
	return new Promise( function ( resolve, reject ) {
		api.get( {
			action: "query",
			prop: "iwlinks|revisions",
			titles: "File:" + filename,
			rvprop: "content",
			rvslots: "*",
			formatversion: 2,
			origin: "*"
		}, {
			xhrFields: { withCredentials: false }
		} ).then( function ( commonsRes ) {
			var page = commonsRes.query.pages[ 0 ];
			if ( page.missing ) {
				resolve( null );
			} else {
				var transcriberData = {};
				page.iwlinks.some( function ( link ) {
					if ( link.prefix === "iarchive" ) {
						transcriberData.iaId = link.title;
						return true; // short circuit
					}
					return false;
				} );
				var match = page.revisions[ 0 ].slots.main.content.match( /<!-- ia-imagedata (.+?) -->/ );
				if ( match ) {
					transcriberData.iiifImageData = JSON.parse( match[ 1 ] );
				}
				resolve( transcriberData );
			}
		} ).catch( reject );
	} );
}

function fixPagenums() {
	document.querySelector( ".ws-noexport" ).getAttribute( "style", null ).append( "<br>" );
}

function addTransliterationForPages( mw ) {
	var api = getTransliterateApi( mw );
	document.querySelectorAll( ".ws-pagenum" ).each( function () {
		var page = this.getAttribute( "title" );
		var startSpan = this.parents( ".ws-noexport" ).first().parent();
		var nextSpan = startSpan.nextAll( "span:has('.ws-pagenum')" ).first();
		if ( nextSpan.length ) {
			addTransliteration( page, nextSpan, "insertBefore", api );
		} else {
			addTransliteration( page, startSpan.parent(), "appendTo", api );
		}
	} );
}

function getTransliterateApi( mw ) {
	return new mw.Api();
}

function transcriberView( filename, mw ) {
	var iaFile = getIaFile( filename );
	var pagenum = 0;

	var wikipages = document.querySelector( ".prp-index-pagelist a" ).get().map( function ( elt ) {
		var href = elt.href;
		var match = href.match( /\/wiki\/(.+)$/ );
		if ( match ) {
			return decodeURIComponent( match[ 1 ] );
		}
		match = href.match( /\btitle=(.+?)(?=&|$)/ );
		if ( match ) {
			return decodeURIComponent( match[ 1 ] );
		}
		return null;
	} );

	getCommonsData( filename, pagenum, mw ).then( function ( data ) {
		if ( !data ) {
			return;
		}
		window.transcriberData = {
			mode: "view",
			commonsFile: filename,
			wikipages: wikipages,
			iaFile: iaFile,
			iaId: data.iaId,
			leaf: pagenum,
			iiifBaseUrl: "https://iiif.archivelab.org/iiif",
			iiifImageData: data.iiifImageData,
			script: "bali",
			language: "ban-bali",
			variant: variant
		};
		transcriberLoad();
	} );
}

function fixMainCss() {
	var sheet = document.styleSheets[ 0 ];
	sheet.insertRule( ".mw-body-content [lang='ban-Bali'] p { word-break: break-all !important; overflow-wrap: anywhere !important; }" );
}

const wrapperFunctions = ( mw ) => {
	mw.hook( "wikipage.content" ).add( function () {
		var contentModel = mw.config.get( "wgPageContentModel" );
		var action = mw.config.get( "wgAction" );
		var title = mw.config.get( "wgTitle" );

		if ( contentModel === "proofread-page" ) {
			fixProofreadPageCss();
			if ( action === "view" ) {
				getTransliterationVariant( mw );
				addTransliteration( mw.config.get( "wgPageName" ), document.querySelectorAll( ".pagetext .mw-parser-output" ), "appendTo", getTransliterateApi() );
			} else if ( action === "edit" ) {
				mw.loader.using( "mediawiki.ForeignApi", function () {
					getTransliterationVariant( mw );
					transcriberEdit( title, mw );
				} );
			}
		} else if ( contentModel === "proofread-index" && action === "view" ) {
			mw.loader.using( "mediawiki.ForeignApi", function () {
				getTransliterationVariant( mw );
				transcriberView( title );
			} );
		} else if ( title.match( /^Ban\// ) && action === "view" && mw.config.get( "wgCanonicalNamespace" ) === "" ) {
			fixPagenums();
			fixMainCss();
			getTransliterationVariant();
			addTransliterationForPages( mw );
		}
	} );
};

Promise.all( [
	// Resource loader modules
	window.mw && wrapperFunctions( window.mw ),
	// Page ready
	document.readyState === "complete"
] ).then( function () {
	if ( window.mw && !window.mw.user.options.get( "proofreadpage-horizontal-layout" ) ) {
		const switchLayout = document.querySelector( ".oo-ui-icon-switchLayout" );
		if ( switchLayout ) {
			switchLayout.click();
		}
	}

	// If you want your app to work offline and load faster, you can change
	// unregister() to register() below. Note this comes with some pitfalls.
	// Learn more about service workers: https://bit.ly/CRA-PWA
	serviceWorker.unregister();
} );
